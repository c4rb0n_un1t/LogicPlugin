#pragma once

#include <QtCore>

#include "../../Interfaces/Utility/ilogicplugin.h"

class LogicPlugin : public QObject, public ILogicPlugin
{
	Q_OBJECT
	Q_INTERFACES(ILogicPlugin)
	Q_PROPERTY(QString testString READ testString WRITE setTestString NOTIFY testStringChanged)
	
public:
	LogicPlugin(QObject* parent);
	virtual ~LogicPlugin() override;
	
	// ILogicPlugin interface
public slots:
	QString testString() override;
	void setTestString(QString testString) override;
	
signals:
	void testStringChanged(QString testString) override;
	
private:
	QString m_testString;
};
